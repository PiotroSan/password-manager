from django.urls import reverse
from django.views.generic import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from credentials.forms import CredentialsForm
from credentials.models import Credentials
from credentials.tools import (
    decrypt_password, encrypt_pasword,
    generate_key, encrypt_base64, decoded_base64,
)


class CredentialsList(ListView):
    model = Credentials
    form_class = CredentialsForm
    template_name = 'credentials/credentials_list.html'
    context_object_name = 'credentials_list'
    paginate_by = 30

    def get_queryset(self):
        return Credentials.objects.filter(user=self.request.user)


class CredentialsEdit(UpdateView):
    model = Credentials
    template_name_suffix = '_update_form'
    fields = ['site_name', 'site_url', 'account_name', 'enckrypt_kind', '_password']

    def form_valid(self, form):
        if self.request.method == 'POST':
            form.instance.user = self.request.user
            if form.instance.enckrypt_kind == Credentials.OWN:
                form.instance._password = encrypt_pasword(form.instance._password)
            else:
                priv_key, pub_key = generate_key()
                form.instance._password = encrypt_base64(
                    form.instance._password, pub_key)

    def get(self, *arg, **kwargs):
        self.object = self.get_object()
        print('obj', self.object.__dict__)
        if self.object.enckrypt_kind == Credentials.OWN:
            print('weszlo')
            self.object._password = decrypt_password(
                self.object._password)
            print('obj', self.object.__dict__)
        else:
            priv_key, pub_key = generate_key()
            self.object._password = decoded_base64(
                self.object._password, priv_key)
        return super(CredentialsEdit, self).get(self, *arg, **kwargs)


class CredentialsNew(CreateView):
    model = Credentials

    def form_valid(self, form):
        form.instance.user = self.request.user
        if form.instance.enckrypt_kind == Credentials.OWN:
            form.instance._passowrd = encrypt_pasword(form.instance._passowrd)
        else:
            priv_key, pub_key = generate_key()
            form.instance._passowrd = encrypt_base64(
                form.instance._passowrd, pub_key)
        return super(CredentialsNew, self).form_valid(form)