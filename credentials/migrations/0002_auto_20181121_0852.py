# Generated by Django 2.1.3 on 2018-11-21 08:52

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('credentials', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='credentials',
            name='enckrypt_kind',
            field=models.CharField(choices=[('O', 'Own solution'), ('B', 'Base64')], default='O', max_length=1),
        ),
        migrations.AddField(
            model_name='credentials',
            name='user',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='credentials', to=settings.AUTH_USER_MODEL),
        ),
    ]
