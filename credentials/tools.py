import re
import base64

from Crypto import Random
from Crypto.PublicKey import RSA


from credentials.enums import SPLITER_MAP


def _spliter_map(part):
    return part + SPLITER_MAP.get(part[0])


def _split_encyrpt(encrypt):
    return re.split(
        r'[{}]'.format(
            ''.join(SPLITER_MAP.values())
        ), encrypt)


def encrypt_pasword(password):
    binary = password.encode('utf-8')
    result = ''
    for part in [repr(char)[::-1] for char in binary]:
        result += _spliter_map(part)
    return result


def decrypt_password(encrypt_password):
    parts = _split_encyrpt(encrypt_password)
    return ''.join(map(chr, [int(char[::-1]) for char in parts if char]))


def generate_key():
    key_legnt = 256*6
    priv_key = RSA.generate(key_legnt, Random.new().read)
    pub_key = priv_key.publickey()
    return priv_key, pub_key


def encrypt_base64(password, pub_key):
    encrypted_password = pub_key.encrypt(password, 64)[0]
    return base64.b64encode(encrypted_password)


def decoded_base64(encrypt, priv_key):
    decoded_password = base64.b64decode(encrypt)
    return priv_key.decrypt(decoded_password)
