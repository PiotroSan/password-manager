from django import forms

from credentials.models import Credentials
from credentials.tools import encrypt_pasword, encrypt_base64, generate_key


class CredentialsForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput(), required=True)

    class Meta:
        model = Credentials
        fields = ['site_name', 'site_url', 'account_name', 'enckrypt_kind', 'user']

    def __init__(self, *args, **kwagrs):
        # instance = kwagrs['instance']
        # if instance:
        #     instance.password = instance._password
        #     kwagrs['instance'] = instance
        self.user = kwagrs.get('user', None)
        super(CredentialsForm, self).__init__(self, *args, **kwagrs)

    def clean_password(self):
        if self.cleaned_data['enckrypt_kind'] == Credentials.OWN:
            return encrypt_pasword(self.cleaned_data['password'].strip())
        priv_key, pub_key = generate_key()
        return encrypt_base64(self.cleaned_data['password'].strip(), pub_key)

    def clean_user(self):
        return self.user
