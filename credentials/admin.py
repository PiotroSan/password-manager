from django.contrib import admin

from credentials.models import Credentials


class CredentialAdmin(admin.ModelAdmin):
    pass


admin.site.register(Credentials, CredentialAdmin)
