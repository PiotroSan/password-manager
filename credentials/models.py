from django.db import models
from django.contrib.auth.models import User

from credentials.tools import decrypt_password, encrypt_pasword, generate_key, encrypt_base64, decoded_base64


class Credentials(models.Model):
    BASE_64 = 'B'
    OWN = 'O'
    ENCRYPT_CHOICES = (
        (OWN, 'Own solution'),
        (BASE_64, 'Base64'),
    )

    enckrypt_kind = models.CharField(
        max_length=1,
        choices=ENCRYPT_CHOICES,
        default=OWN,
        null=False,
        blank=False)
    site_name = models.CharField(max_length=200, null=False, blank=False)
    site_url = models.URLField(max_length=255)
    account_name = models.CharField(max_length=200, null=False, blank=False)
    _password = models.CharField(max_length=255, null=False, blank=False)
    user = models.ForeignKey(
        User,
        related_name='credentials',
        on_delete=models.CASCADE,
        default=None)

    @property
    def password(self):
        if self.enckrypt_kind == self.OWN:
            return decrypt_password(self._password)
        priv_key, pub_key = generate_key()
        return decoded_base64(self._password, priv_key)

    @password.setter
    def password(self, value):
        if self.enckrypt_kind == self.OWN:
            self._password = encrypt_pasword(value)
        priv_key, pub_key = generate_key()
        self._password = encrypt_base64(value, pub_key)
