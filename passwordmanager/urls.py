"""passwordmanager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.contrib.auth.decorators import login_required
from django.contrib.auth import views as auth_views

from credentials.views import CredentialsList, CredentialsEdit, CredentialsNew

urlpatterns = [
    path('admin/', admin.site.urls),
    path('list/', login_required(CredentialsList.as_view()), name='credentials-list'),
    path('details/<int:pk>/', login_required(CredentialsEdit.as_view()), name='credential-details'),
    path('new/', login_required(CredentialsNew.as_view()), name='credential-new'),
    path('login/', auth_views.LoginView.as_view(), name='login'),
]
